import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import Sub from './components/Sub';

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Sub />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  
});
