import React, { Component } from "react";
import { Picker } from "react-native";
import { Card, CardSection, Button, Input } from "./common";
class Sub extends Component {
  constructor(props) {
    super(props);
    this.state = {
      a: "",
      b: "",
      beer: "",
      arrayBeer: [
        { name: "Tiger", key: 20},
        { name: "333", key:  18 },
        { name: "Larue", key: 15 }
      ]
    };
  }
  checkNumber = (text, begin, end) => {
    if (!parseInt(text) || parseInt(text).toString() != text || parseInt(text) < begin || parseInt(text) > end) {
      return false
    }else{
      return true
    }
  };
  
  updateBeer = beer => {
    this.setState({ beer });
  };
  handleResult = (a, b, beer) => {
    const { checkNumber } = this;
    if (checkNumber(a, 1, 200) && checkNumber(b, 1, 24)) {
      let sub = parseInt(a) - parseInt(b);
      alert(sub < beer ? "Declined!!!" : "Accepted!!!");
    }else{
      alert(`Number is out of range oror format fail`);

    }
  }
  onPress = () => {
    const { a, b, beer } = this.state;
    if (!a || !b) {
      alert("You need enter full information");
    }else{
      this.handleResult(a, b, beer);
    }
  };

  render() {
    const { arrayBeer } = this.state;
    return (
      <Card>
        <CardSection>
          <Input
            label="Age"
            placeholder="Enter age"
            onChangeText={text => {
              this.setState({ a: text });
            }}
            value={this.state.a}
          />
        </CardSection>
        <CardSection>
          <Input
            label="Bottles"
            placeholder="Enter amount of bottles"
            onChangeText={text => {
              this.setState({ b: text });
            }}
            value={this.state.b}
          />
        </CardSection>
        <CardSection>
          <Picker
            style={{ width: 300 }}
            selectedValue={this.state.beer}
            onValueChange={this.updateBeer}
          >
            {arrayBeer.map(e => (
              <Picker.Item label={e.name} value={e.key} key={e.key} />
            ))}
          </Picker>
        </CardSection>
        <CardSection>
          <Button onPress={this.onPress}>Submit</Button>
        </CardSection>
      </Card>
    );
  }
}

export default Sub;
